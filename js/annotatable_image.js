/**
 * @file
 */

(function ($) {

  var annotatable_images_counters = new Array();

  function annotatable_counters(id, reset) {
    if (typeof annotatable_images_counters[id] == 'undefined') {
      annotatable_images_counters[id] = 0;
    }

    if (typeof reset == 'undefined') {
      annotatable_images_counters[id]++;
    }
    else {
      annotatable_images_counters[id] = 0;
    }

    return annotatable_images_counters[id];
  }

  Drupal.behaviors.prp_annotation_edit = {
    attach: function (context, settings) {
      $('.annotatable-image:not(.annotation-processed)').each(function() {
        var id = $(this).attr('id');

        function blackNote() {
          var annotated_count = annotatable_counters(id);

          return $(document.createElement('span')).addClass('black circle note').html(annotated_count).click(function() {
            var element_container = $(this).parent('.annotatable-image');
            $(this).remove();

            var serialized_annotations = $('span.note', element_container).seralizeAnnotations();
            var serialized_coordinates = JSON.stringify(serialized_annotations);

            $('span.note', element_container).remove();
            annotatable_counters(id, true);
            jQuery('textarea', element_container).text(serialized_coordinates);
            element_container.addAnnotations(blackNote, serialized_annotations);
          });
        }

        $(this).annotatableImage(blackNote);
        $(this).mousedown(function() {
          var serialized_annotations = $('span.note', this).seralizeAnnotations();
          if (serialized_annotations) {
            var serialized_coordinates = JSON.stringify(serialized_annotations);
            jQuery('textarea', this).text(serialized_coordinates);
          }
        });

        if (typeof Drupal.settings.annotation_points_edit != 'undefined' && typeof Drupal.settings.annotation_points_edit[id] != 'undefined') {
          var annotations_edit = jQuery.parseJSON(Drupal.settings.annotation_points_edit[id]);
          if (annotations_edit) {
            annotatable_counters(id, true);
            $(this).addAnnotations(blackNote, annotations_edit);
          }
        }

        $(this).addClass('annotation-processed');
      });
    }
  };

  Drupal.behaviors.prp_annotation_view = {
    attach: function (context, settings) {

      $('.annotatable-image-view').each(function() {
        var counter = 0;
        var image_delta = $(this).attr('id');

        function blackNoteView() {
          counter++;
          return $(document.createElement('span')).addClass('black circle note').html(counter);
        }

        if (typeof Drupal.settings.annotation_points != 'undefined' && typeof Drupal.settings.annotation_points[image_delta] != 'undefined') {
          var annotations = jQuery.parseJSON(Drupal.settings.annotation_points[image_delta]);
          if (annotations) {
            $(this).addAnnotations(blackNoteView, annotations);
          }
        }
      });
    }
  };

  Drupal.behaviors.prp_annotation_remove_all = {
    attach: function (context, settings) {
      $('.annotatable-image-remove-all-annotations:not(.remove-annotation-processed)').each(function() {
        $(this).click(function(event) {
          event.preventDefault();

          var div_to_clean = $(this).parent().prev();
          var img_to_clean = $('div.annotatable-image', div_to_clean);
          $('span.note', div_to_clean).remove();
          annotatable_counters(img_to_clean.attr('id'), true);

          var serialized_annotations = $('span.note', img_to_clean).seralizeAnnotations();
          var serialized_coordinates = JSON.stringify(serialized_annotations);
          jQuery('textarea', img_to_clean).text(serialized_coordinates);
        });

        $(this).addClass('remove-annotation-processed');
      });
    }
  };
}(jQuery));
